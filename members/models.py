from django.db import models
from django.contrib.auth.models import User

''' 
Member model contains information for every member in the system.
This is merely profile to the user auth model.
'''
class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT,unique=True)
    phone = models.CharField(max_length=255, blank=True)
    riot = models.CharField(max_length=255, blank=True)
    mastodon = models.CharField(max_length=255, blank=True)
    blog = models.CharField(max_length=255, blank=True)
    gpg = models.TextField(blank=True)
    about = models.TextField(blank=True)
    institution = models.CharField(max_length=255, blank=True)
    dob = models.DateField(blank=True,null=True)
    ham_handle = models.CharField(max_length=255, blank=True)
    profile_pic = models.ImageField(blank=True)

    def __str__(self):
        return self.user.username