# Ludus

Ludus (loo-das) is a participatory web platform for communities of user groups, enthusiasts, hobbyists, movements, advocacy etc.

## LICENSE

AGPLv3 

## Architecture Design

### Tech Stack

Django based modular web application

### Technologies to be implemented

* ActivityPub
* OpenBadges
* Progressive Web Application