from django.db import models

'''
Model field for the organization in one language.
The language is the same as the i18n code.
'''


class Organization(models.Model):
    name = models.CharField(max_length=255)
    abbreviation = models.CharField(max_length=255, blank=True)
    email = models.EmailField()
    phone = models.CharField(max_length=255, blank=True)
    riot = models.CharField(max_length=255, blank=True)
    blog = models.CharField(max_length=255, blank=True)
    mastodon = models.CharField(max_length=255, blank=True)
    about = models.TextField(blank=True)
    dob = models.DateField(blank=True, null=True)
    lang = models.CharField(max_length=255, blank=True)
    address = models.CharField(max_length=255, blank=True)
    bank = models.CharField(max_length=255, blank=True)
    logo = models.ImageField(blank=True)
    favicon = models.ImageField(blank=True)
    website = models.CharField(max_length=255, blank=True)
    git_repo = models.CharField(max_length=255, blank=True)
    wiki = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.name
